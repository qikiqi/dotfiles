set shell=/bin/bash

set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'gkjgh/cobalt'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Obey PEP8
au BufNewFile,BufRead *.py;
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

au BufNewFile,BufRead *.cc;
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2
    \ set fileformat=unix

"define BadWhitespace before using in a match
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py match BadWhitespace /\s\+$/
set encoding=utf-8

let python_highlight_all=1
syntax on

colorscheme cobalt

set nu
