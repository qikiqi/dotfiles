# Dotfiles

My dotfiles

![scrot](.dotfiles/recent.png)


## Setup

- Application Launcher: `rofi`
- Music Player: `cmus / spotify`
- Notifications: `dunst`
- Shell: `fish`
- Terminal Emulator: `urxvt`
- Text Editor: `nvim`
- Video Player: `mpv`
- Web Browser: `firefox`
- Window Manager: `i3-gaps`

## Repository

[Repository configured as described here](https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/ "The best way to store your dotfiles: A bare Git repository")

