#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export EDITOR='nvim'
export VISUAL='nvim'
# Disabled now since i3-sensible-terminal
#   launches urxvt by default if this not set
#export TERMINAL='alacritty'
#export QT_STYLE_OVERRIDE=adwaita
export QT_QPA_PLATFORMTHEME="qt5ct"
export KUBECONFIG=$KUBECONFIG:~/.kube/config:~/.kube/pk3s-1-config:~/.kube/k3s-config:~/.kube/k3s-ci-asa

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh

# The current way of starting x without ssh-agent
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi
eval $(keychain --eval --quiet bitbucket_key github_noreply lyta_aalto git_aalto id_w520 id_w520_root id_gitlab_10.90.60.138 id_kmikkola_ophelia gitlab-flu gitlab-k3s gitlab-pk3s-1 id_gitlab-empty1 gitlab-thesis-k3s gitlab-kmo id_xk3s_g gitlab-elastic gitlab-harbor kosh_aalto id_empty1)
exec fish
