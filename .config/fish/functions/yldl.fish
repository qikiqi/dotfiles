function yldl
	set ylefolder "/hdd/yle/"
	echo $ylefolder
	# Clear the folder
	if [ "$argv[1]" = "remove" ]
		ll /hdd/yle/
		echo "Downloads cleared"
		rm /hdd/yle/*
		ll /hdd/yle/
	# Temporary downloader without proper mkv container with subtitles
	# Possible to watch the old format while downloading
	# New format needs to be completely downloaded in order to start watching	
	else if [ "$argv[1]" = "d" ]
		set uniqname (date +'%s')
		yle-dl -V --backend youtubedl "$argv[2]" -o /hdd/yle/$uniqname
	# This parses subtitles and file to and mkv container
	# Works for old and new format
	# As soon as mkv is created file can be played
	else if [ "$argv[1]" = "e" ]
		set ffmpegcmd (python /home/x230/sync/oma/scripts/arch/scripts/downloadareenasubs.py "$argv[2]" $ylefolder)
		eval $ffmpegcmd[1]
		if [ $ffmpegcmd[2] ]
			rm $ffmpegcmd[2].mp4
			rm $ffmpegcmd[2].flv
		end
	# This pipes the new format directly to mpv with or without subtitles
	else
		set vodurl (yle-dl --showurl "$argv[1]")
		set uniqname (date +'%s')
		# Since yle-dl doesnt support downloading ONLY subtitles
		# It shall be done via python script
		set subfile (python /home/x230/sync/oma/scripts/arch/scripts/streamareena.py "$argv[1]" $ylefolder)
		if [ -z $subfile[1] ]
			mpv $vodurl
		else
			mpv "$vodurl" --sub-file="$subfile"
		end
	end
end
