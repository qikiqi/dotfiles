set -g theme_date_format "+%Y-%m-%d %H:%M:%S"
set -g theme_nerd_fonts yes
source /usr/share/fish/completions/conda.fish
