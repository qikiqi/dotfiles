#
# ~/.bash_profile
#
export QT_AUTO_SCREEN_SCALE_FACTOR=1
[[ -f ~/.bashrc ]] && . ~/.bashrc
[ "$XDG_CURRENT_DESKTOP" = "KDE" ] || [ "$XDG_CURRENT_DESKTOP" = "GNOME" ] || export QT_QPA_PLATFORMTHEME="qt5ct"
